//@TODO: on orientation change refresh banner;
var advertTimer =0;
var adReward = false;
var backSpaceCounter = 0;
var replaceCounter = 0;
var queueCounter = 0;
var symbolQueueArray = [];
var numOfAllowedSymbolsInQueue = Math.round(screen.width / 70);
var spaceCharacter = "_Space";
var fullStopCharacter = "_FullStop";
var popMenuOpen = false;
var rewardPopOpen = false;
var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

// Initialize your app
var myApp = new Framework7();
// Export selectors engine
var $$ = Dom7;




function Button_Translate(){
    var printList = "";
    if(symbolQueueArray.length==0){
        printList="You haven't selected any symbols to translate.";
    }
    for(var i =0;i<symbolQueueArray.length;i++){
        if(symbolQueueArray[i]==spaceCharacter){
            printList+=" ";
        }
        else if(symbolQueueArray[i]==fullStopCharacter){
            printList+=".";
        }
        else{
            if(symbolQueueArray[i]){
                printList+=symbolQueueArray[i];
            }
        }
    }
    $('#OUTPUT_translation').empty().append(printList);
    printList = printList.split("");
    $('#OUTPUT_symbols').empty();
    printList.forEach(function(letter) {
        $('#OUTPUT_symbols').append('<a class="sheikahText">'+letter+'</a>');
    });
}
function Button_Backspace(){
    //i dont remeber if all this is needed, this is from when i used images to represent the characters.
    if(queueCounter>0){
        var tempBackSpace = backSpaceCounter-1;
        if(symbolQueueArray.length-tempBackSpace>=0){
            delete symbolQueueArray[symbolQueueArray.length+tempBackSpace];
            backSpaceCounter--;
        }
        queueCounter--;
        var tempElement = document.getElementById("queueImg"+queueCounter.toString());
        tempElement.parentNode.removeChild(tempElement);
        //display latest replaced element and denumerate replaceCounter
    }
}
function Button_ClearAll(){
    //clear all from div and place a new placeholder
    $('#textQueue').empty();
    //reset all variables
    backSpaceCounter = 0;
    replaceCounter = 0;
    queueCounter = 0;
    symbolQueueArray = [];
}

function imageOnClick(aLetter){

    if(symbolQueueArray.length!=0){
        if(symbolQueueArray[symbolQueueArray.length-1]===undefined){
            //now add in the element to the first undefined slot in the list
            for(var i=0;i<symbolQueueArray.length;i++){
                if(symbolQueueArray[i]){
                    //alert("defined at"+i);
                }
                else{
                    //alert("undefined at"+i);
                    symbolQueueArray[i]= aLetter;
                    i=symbolQueueArray.length;
                    backSpaceCounter++;
                }
            }
        }
        else{
            symbolQueueArray[symbolQueueArray.length] = aLetter;
            //alert(backSpaceCounter);
        }
    }
    else{
        symbolQueueArray[symbolQueueArray.length] = aLetter;
        if(backSpaceCounter!=0){
            backSpaceCounter++;
        }
    }
    $('#textQueue').append('<a class="sheikahText" id="queueImg'+ queueCounter+'">'+aLetter+'</a>');
    queueCounter++;
    //alert(symbolQueueArray.length);
}
//AdMob
//initialize the goodies 

/*
     banner: 'ca-app-pub-8770565591901781/6053273355',		//PUT ADMOB ADCODE HERE 
     interstitial: 'ca-app-pub-8770565591901781/1483472955'

*/

//display the interstitial 
function showInterstitialFunc(){
    if(!adReward){
        if(advertTimer==0){
            admob.interstitial.show();
            function getRandomIntInclusive(min, max) {
              min = Math.ceil(min);
              max = Math.floor(max);
              return Math.floor(Math.random() * (max - min + 1)) + min;
            }
            admob.interstitial.prepare();
            advertTimer = getRandomIntInclusive(2,3);
            admob.banner.prepare();
            console.log("logMsg: display option");
            rewardPopOpen = true;
            myApp.popup.open('.popup-rewardPrompt');

        }
        else{
            advertTimer--;
        }
    }
    else{
        //occurs when user has watched the reward video.
    }
}
function openRewardVideo(){
    admob.rewardvideo.show();
}
function noRewardVideo(){
    rewardPopOpen=false;
    myApp.popup.close('.popup-rewardPrompt');
    return;
}
$('#input_VCC').empty().append('<a class="sheikahText textButtons" onclick="imageOnClick(\'' + spaceCharacter + '\')">&nbsp</a>');
$('#input_VCC').append('<a class="sheikahText textButtons" onclick="imageOnClick(\'' + fullStopCharacter + '\')">.</a>');
$('#input_CC').empty().append('<p>Below are common characters</p>');
alphabet.forEach(function(letter) {
    $('#input_CC').append('<a class="sheikahText textButtons" onclick="imageOnClick(\'' + letter + '\')" >'+letter+'</a>');
});
$('#input_N').empty().append('<p>Below are the Number symbols</p>');
for(var i = 0;i<=9;i++){
    $('#input_N').append('<a class="sheikahText textButtons" onclick="imageOnClick(\'' + i + '\')">'+i+'</a>');
}
    //$('#Input_CC').append('<img src="img/zeldaSheikah/A.png" height="64px" width="64px">');


$("#Button_Translate").click(function(){
    myApp.popup.open('.popup-translation');
    popMenuOpen = true;
});
document.addEventListener("deviceready", function(){
    document.addEventListener("backbutton", function(e){
        if(popMenuOpen){
            popMenuOpen = false;
            showInterstitialFunc();
            myApp.popup.close('.popup-translation');
            return;
        }
        if(rewardPopOpen){
            rewardPopOpen=false;
            myApp.popup.close('.popup-rewardPrompt');
            return;
        }
        else{
            myApp.confirm('Are you sure?','Exit', function () {
               navigator.app.exitApp();
               return;
           });

        }
    }, false);
    try{

    }catch(e){
        console.log("logMsg: "+e);
    }
    admob.banner.config({
     id: 'ca-app-pub-8770565591901781/6053273355',
     isTesting: false,
     autoShow: true,
     overlap: true
    });
    admob.banner.prepare();
    admob.interstitial.config({
     id: 'ca-app-pub-8770565591901781/1483472955',
     isTesting: false,
     autoShow: false
    });
    admob.interstitial.prepare();
    admob.rewardvideo.config({
     id: 'ca-app-pub-8770565591901781/3337275013',
    });

    admob.rewardvideo.prepare();


    document.addEventListener('admob.banner.events.LOAD_FAIL',function(event){
        admob.banner.prepare();
    });
    document.addEventListener('admob.banner.events.LOAD',function(event){

    });

    document.addEventListener('admob.interstitial.events.LOAD_FAIL',function(event){
        console.log("logMsg: interstitial load fail: "+JSON.stringify(event));
    });
    document.addEventListener('admob.interstitial.events.LOAD',function(event){
        console.log("logMsg: interstitial loaded");
    });
    document.addEventListener('admob.interstitial.events.CLOSE',function(event){
     //does not fire. ever...

    });

    document.addEventListener('admob.interstitial.events.OPEN',function(event){
        //does not fire. ever..
    });

    document.addEventListener('admob.rewardvideo.events.LOAD',function(event){
        //alert("reward video has loaded");
    });
    document.addEventListener('admob.rewardvideo.events.LOAD_FAIL',function(event){
        //alert("reward video load fail");
    });
    document.addEventListener('admob.rewardvideo.events.OPEN',function(event){
        //
    });
    document.addEventListener('admob.rewardvideo.events.CLOSE',function(event){
        console.log("logMsg: closed reward video");
    });
    document.addEventListener('admob.rewardvideo.events.EXIT_APP',function(event){
        console.log("logMsg: rewardVideo ,exit_App");
    });
    document.addEventListener('admob.rewardvideo.events.START',function(event){
        //code
    });
    document.addEventListener('admob.rewardvideo.events.REWARD',function(event){
        console.log("logMsg: reward trigger");
        myApp.popup.close('.popup-rewardPrompt');
        adReward=true;
        rewardPopOpen=false;
        admob.banner.remove();
        return;
    });
}, false);
