/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.torelkarakurtapps.SheikahTranslatorFree;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.torelkarakurtapps.SheikahTranslatorFree";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10008;
  public static final String VERSION_NAME = "1.0.08";
}
